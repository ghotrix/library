#!/usr/bin/env python3

import os

if not os.environ.get('DB_CREATED'):
    import db_create
    os.environ['DB_CREATED'] = '1'
from app import app

#app.run(debug=True, port=int(os.environ.get('PORT', 5000)))
