from config import SQLALCHEMY_DATABASE_URI
from app import db, models
import os.path
db.create_all()

books = [['Alice in Wonderland'], ['Chronicles of Amber', 'A Night in the Lonesome October'], ['The C Programming Language']]
authors = [['L. Carroll'], ['R. Zelazny'], ['Kernighan', 'Ritchie']]

usernames = ['simple_user', 'cool_user']
passwords = ['qweasd', 'qwerty']
roles = [0, 1]

for i, authors_by_books in enumerate(authors):
    for author_name in authors_by_books:
        author = models.Author(name=author_name)
        for book_name in books[i]:
            book = models.Book.query.filter_by(name=book_name).first() or models.Book(name=book_name)
            author.books.append(book)
            db.session.add(book)
            db.session.flush()
        db.session.add(author)

for i, username in enumerate(usernames):
    user = models.User(name=username, password=passwords[i], role=roles[i])
    db.session.add(user)

db.session.commit()
