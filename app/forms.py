from flask.ext.wtf import Form
from wtforms.fields import TextField, BooleanField, PasswordField, DecimalField, HiddenField, SelectMultipleField
from wtforms.validators import Required

class LoginForm(Form):
    login = TextField('login', validators = [Required()])
    password = PasswordField('password', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)

class BookForm(Form):
    name = TextField('Book name:', validators = [Required()])

class AuthorForm(Form):
    name = TextField('Author name:', validators = [Required()])
    books = SelectMultipleField('Books', coerce=int)
