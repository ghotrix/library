#!/usr/bin/env python3

from flask import Flask, render_template, request, flash, redirect, url_for, session
from flask.ext.login import LoginManager, login_user, logout_user, current_user
from app import app, db, models
from app.forms import BookForm, AuthorForm, LoginForm

lm = LoginManager()
lm.init_app(app)

@lm.user_loader
def load_user(id):
    return models.User.query.get(int(id))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('books'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = models.User.query.filter(models.User.name==form.login.data).first()
        if user != None and user.password == form.password.data:
            session['remember_me'] = form.remember_me.data
            login_user(user, remember=session['remember_me'])
    return redirect(request.args.get('next'))

@app.route('/')
@app.route('/books')
def books():
    form = LoginForm()
    books = models.Book.query.all()
    return render_template('books.html', books=books, login_form=form)

@app.route('/authors')
def authors():
    form = LoginForm()
    authors = models.Author.query.all()
    books_by_authors = []
    for author in authors:
        books_for_author = models.Book.query.filter(models.Book.authors.any(name=author.name))
        books_by_authors.append((author, books_for_author))
    return render_template('authors.html', books=books_by_authors, login_form=form)

@app.route('/add_author', methods=['GET', 'POST'])
def add_author():
    if not current_user.role > 0:
        return lm.unauthorized()
    form = AuthorForm(request.form)
    form.books.choices = [(b.id, b.name) for b in models.Book.query.order_by('name')]
    if request.method == 'POST' and form.validate():
        author = models.Author(form.name.data)
        books_id = form.books.data
        books = models.Book.query.filter(models.Book.id.in_(books_id)).all()
        author.books = books
        db.session.add(author)
        db.session.commit()
        return redirect(url_for('authors'))
    return render_template('add_author.html', action='/add_author',
            value='Add author to the lib', form=form)

@app.route('/edit_author/<int:author_id>', methods=['GET', 'POST'])
def edit_author(author_id):
    if not current_user.role > 0:
        return lm.unauthorized()
    author = models.Author.query.filter(models.Author.id==author_id).first()
    form = AuthorForm(obj=author)
    form.books.choices = [(b.id, b.name) for b in models.Book.query.order_by('name')]
    if request.method == 'POST' and form.validate():
        author.name = form.name.data
        books_id = form.books.data
        books = models.Book.query.filter(models.Book.id.in_(books_id)).all()
        author.books = books
        db.session.commit()
        return redirect(url_for('authors'))
    
    form.books.data = [book.id for book in author.books]
    return render_template('add_author.html', action='/edit_author/',
            value='Change author in the lib', id=author_id, form=form)

@app.route('/add_book', methods=['GET', 'POST'])
def add_book():
    if not current_user.role > 0:
        return lm.unauthorized()
    form = BookForm(request.form)
    if request.method == 'POST' and form.validate():
        book = models.Book(form.name.data)
        db.session.add(book)
        db.session.commit()
        return redirect(url_for('books'))
    return render_template('add_book.html', action='/add_book',
            value='Add book to the lib', form=form)

@app.route('/edit_book/<int:book_id>', methods=['GET', 'POST'])
def edit_book(book_id):
    if not current_user.role > 0:
        return lm.unauthorized()
    book = models.Book.query.filter(models.Book.id==book_id).first()
    form = BookForm(obj=book)
    if request.method == 'POST' and form.validate():
        book.name = form.name.data
        db.session.commit()
        return redirect(url_for('books'))
    return render_template('add_book.html', action='/edit_book/',
            value='Change book in the lib', id=book_id, form=form)

@app.route('/remove_book/<int:book_id>', methods=['GET', 'POST'])
def remove_book(book_id):
    if not current_user.role > 0:
        return lm.unauthorized()
    book = models.Book.query.get(book_id)
    for author in book.authors:
        book.authors.remove(author)
    db.session.delete(book)
    db.session.commit()
    return redirect(url_for('books'))

@app.route('/remove_author/<int:author_id>', methods=['GET', 'POST'])
def remove_author(author_id):
    if not current_user.role > 0:
        return lm.unauthorized()
    author = models.Author.query.get(author_id)
    for book in author.books:
        author.books.remove(book)
    db.session.delete(author)
    db.session.commit()
    return redirect(url_for('authors'))

if __name__ == "__main__":
    app.run(debug=True)
